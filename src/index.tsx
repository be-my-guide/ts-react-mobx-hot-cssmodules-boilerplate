/**
 * App entry point
 */
import * as React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { createStore } from './store';
import { App } from './App';

// tslint:disable-next-line
const renderApp = (Component: typeof App) => {
  render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('root'),
  );
};


window.onload = async () => {
  const store = createStore();
  renderApp(App);
};


if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./App', () => {
    const next = require('./App').App;
    renderApp(next);
  });
}

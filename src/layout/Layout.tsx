import * as React from 'react';

const style = require('./style.scss');

/**
 * Website layout
 */
export class Layout extends React.Component {

  render() {

    return(
      <div className={ style.layout }>
        { this.props.children }
      </div>
    );

  }

}

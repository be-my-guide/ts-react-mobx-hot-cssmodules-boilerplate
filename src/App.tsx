import * as React from 'react';
import { Provider } from 'mobx-react';
import { getStore } from './store';
import { Router } from './router/Router';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router';
import { setHistory } from './utils/history';

/**
 * Top Level app component
 */
export class App extends React.PureComponent {


  render() {
    return (
      <Provider store={ getStore() }>
        <BrowserRouter>
          <Route render={ props => {
            setHistory(props.history);
            return <Router
              history={ props.history }
              match={ props.match }
              location={ props.location }
            />;
          }}/>
        </BrowserRouter>
      </Provider>
    );
  }


}

/**
 * Root Store
 */
import { DataStore } from './DataStore';
import { UserStore } from './UserStore';
import { ViewStore } from './ViewStore';

export class Store {

  public static INSTANCE: Store;

  private _data: DataStore;
  private _user: UserStore;
  private _view: ViewStore;

  constructor() {

    if (Store.INSTANCE) {
      throw new Error('Store is a singleton');
    }

    this._data = new DataStore(this);
    this._user = new UserStore(this);
    this._view = new ViewStore(this);

    Store.INSTANCE = this;
  }

}

export interface StoreInjection {
  store?: Store;
}

export function createStore(): Store {
  return new Store();
}

export function getStore(): Store {
  return Store.INSTANCE;
}



import { Store } from './Store';
import { computed, observable } from 'mobx';

/**
 * Data Store keep the view state
 */
export class ViewStore {

  private _rootStore: Store;

  @observable
  private _isReady: boolean = false;


  constructor(rootStore: Store) {
    this._rootStore = rootStore;

    this._isReady = true;
  }

  @computed
  public get isReady(): boolean {
    return this._isReady;
  }

  @computed
  public get location(): string {
    return '/';
  }

}

/**
 * History singleton
 */
import { History } from 'history';

let history: History;

export function setHistory(h: History): void {
  history = h;
}

export function getHistory(): History {
  if (!history) {
    throw new Error('history is missing');
  }

  return history;
}

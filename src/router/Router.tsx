import * as React from 'react';
import { Route, RouteComponentProps } from 'react-router';
import { StoreInjection } from '../store';
import { inject, observer } from 'mobx-react';
import { Layout } from '../layout/Layout';

const log = require('debug')('app:router');


type RouterProps = RouteComponentProps<{}> & StoreInjection;

/**
 * Handles routing
 */
@inject('store')
@observer
export class Router extends React.Component<RouterProps>  {

  render() {
    log('render()', this.props);

    return (
      <Layout>

      </Layout>

    );



  }

}

require('dotenv').config({path: __dirname +  '/.env'});

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const Dotenv = require('webpack-dotenv-plugin');


const PROJECT_ROOT = path.join(__dirname, '..');
const SRC_ROOT = path.join(PROJECT_ROOT, '/src');
const STYLE_ROOT = path.join(SRC_ROOT, '/style');
const DEV_ROOT = path.join(PROJECT_ROOT, '/dev');
const STATIC_ROOT = path.join(PROJECT_ROOT, '/static');

module.exports = {

  devServer: {
    contentBase: STATIC_ROOT,
    publicPath: "/",
    hot: true,
    historyApiFallback: { index: 'index.html' },
    disableHostCheck: true,
    watchContentBase: true,
    stats: "minimal",
    port: process.env.DEV_SERVER_PORT,
    host: process.env.DEV_SERVER_HOST,
  },

  entry: {

    main: [
      "react-hot-loader/patch",
      "react-dev-utils/webpackHotDevClient",
      "./src/index.tsx"
    ],

    style: "./src/style/main.scss"

  },

  output: {
    path: STATIC_ROOT,
    publicPath: "/",
    filename: "[name].js"
  },

// devtool: 'inline-source-map',
// devtool: 'inline-cheap-module-source-map',
  devtool: 'cheap-module-eval-source-map',
// devtool: 'source-map',


  resolve: {
    extensions: [".js", ".json", ".ts", ".tsx", ".sass", ".scss", ".css"],

    modules: [
      "./src",
      "node_modules"
    ],

    plugins: [
      new TsConfigPathsPlugin()
    ]
  },


  module: {

    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        loaders: [
          // "source-map-loader",
        ]
      },

      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader',
        options: {
          // configFileName: path.join(PROJECT_ROOT, 'tsconfig.json')
        },
        include: SRC_ROOT
      },

      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=500000'
      },


      // CSS Modules (css, sass, scss)
      {
        test: /\.(sass|scss|css)$/,
        include: SRC_ROOT,
        exclude: STYLE_ROOT,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader?importLoaders=1&modules&localIdentName=[local].[hash:base64:3]',
            'postcss-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: [SRC_ROOT]
              }
            }
          ]
        })
      },

      {
        test: /\.(sass|scss|css)$/,
        include: STYLE_ROOT,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { sourceMap: false } },
            // { loader: 'postcss-loader', options: { sourceMap: false } },
            { loader: 'resolve-url-loader', options: { sourceMap: false } },
            { loader: 'sass-loader', options: { sourceMap: false } }
          ]
        })
      }


    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
// new BundleAnalyzerPlugin({
// 	analyzerMode: 'static'
// }),
    new webpack.SourceMapDevToolPlugin({
      exclude: /node_modules/
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'node_modules',
      filename: 'node_modules.js',
      minChunks(module, count) {
        var context = module.context;
        return context && context.indexOf('node_modules') >= 0;
      }
    }),
    new ExtractTextPlugin({ filename: '[name].css' }),
    new Dotenv({
      sample: path.resolve(DEV_ROOT, '.env'), // Path to .env file (this is the default)
      path: path.resolve(DEV_ROOT, '.env') // Path to .env file (this is the default)
    })
  ]
};

const path = require('path');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require(path.join(__dirname, 'webpack.config.js'));

const compiler = webpack(config);
const port = config.devServer.port;
const host = config.devServer.host;

new WebpackDevServer(compiler, config.devServer).listen(port, host, (err) => {
  if (err)
    console.log(err);
  else
    console.log(`Listening at ${host}:${port}`)
});
